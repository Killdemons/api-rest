function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
var jokeid = getParameterByName('idjoke');

fetch('http://api.icndb.com/jokes/' + jokeid)
.then((respuesta) => {
	return respuesta.json();
}).then((respuesta) => {
	document.getElementById('joke').value = respuesta.value.joke;
})